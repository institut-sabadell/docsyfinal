---
title: Institut Sabadell
---

{{< blocks/cover title="Benvingut a Institut Sabadell!" image_anchor="top" height="full" >}}
<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/">
Sistemes <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-secondary me-3 mb-4" href="https://github.com/google/docsy-example">
Base de Dades <i class="fab fa-github ms-2 "></i>
</a>

<p class="lead mt-5">Modificat per Ruben Collado</p>
{{< blocks/link-down color="info" >}}
{{< /blocks/cover >}}

{{% blocks/lead color="primary" %}}
Per modificar aquesta pagina he utilitzat l'index.md de la carpeta content.

La pagina real es pot trobar a [Docsy](https://docsy.dev))
{{% /blocks/lead %}}

{{% blocks/section color="dark" type="row" %}}
{{% blocks/feature icon="fa-lightbulb" title="Eloi Vazquez" %}}
Professor de web
{{% /blocks/feature %}}

{{% blocks/feature icon="fab fa-github" title="Joan Queralt" url="https://github.com/google/docsy-example" %}}
Professor de Sistemes i Base de dades
{{% /blocks/feature %}}

{{% blocks/feature icon="fab fa-twitter" title="Marc Albareda" url="https://twitter.com/docsydocs" %}}
Cap d'estudis i professor de programacio
{{% /blocks/feature %}}

{{% /blocks/section %}}

{{% blocks/section %}}
This is the second section
{.h1 .text-center}
{{% /blocks/section %}}

{{% blocks/section type="row" %}}

{{% blocks/feature icon="fab fa-app-store-ios" title="Download **from AppStore**" %}}
Get the Goldydocs app!
{{% /blocks/feature %}}

{{% blocks/feature icon="fab fa-github" title="Contributions welcome!"
    url="https://github.com/google/docsy-example" %}}
We do a [Pull Request](https://github.com/google/docsy-example/pulls)
contributions workflow on **GitHub**. New users are always welcome!
{{% /blocks/feature %}}

{{% blocks/feature icon="fab fa-twitter" title="Follow us on Twitter!"
    url="https://twitter.com/GoHugoIO" %}}
For announcement of latest features etc.
{{% /blocks/feature %}}

{{% /blocks/section %}}

{{% blocks/section %}}
This is the another section
{.h1 .text-center}
{{% /blocks/section %}}
